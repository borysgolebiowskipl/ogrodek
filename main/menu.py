import pickle
from posiadlosc import *
import os
import sys


class Menu:
    def __init__(self, posiadlosc):
        self.posiadlosc = posiadlosc
        self.szerokosc = 37
    ##################################################################
    # test
    def cls(self):
       if os.name == 'posix':
          os.system('clear')
       else:
          os.system('cls')


    def pokaz_menu(self, menu):
        self.cls()
        tytul = ' ' + menu['tytul'] + ' '
        ile_z_lewej = int((self.szerokosc - len(tytul)) / 2) -1
        tytul = '╔' + '═' * ile_z_lewej + tytul
        tytul = tytul.ljust(self.szerokosc-1, '═') + '╗'
        print(tytul)
        
        for opcja in menu['opcje']:
            linia = f'║ {opcja[0]}. {opcja[1]}'.ljust(self.szerokosc-1) + '║'
            print(linia)
        
        print('╚' + '═' * (self.szerokosc-2) + '╝')
        
        wybor = input('Podaj akcję do wykonania:')
        akcja = None
        for opcja in menu['opcje']:
            if str(opcja[0]) == wybor:
                akcja = opcja
        
        if akcja == None:
            print('Niewłaściwy wybór!')
            input('Naciśnij Enter ...')
            self.pokaz_menu(menu)
        else:
            if len(akcja) == 2:
                print('Opcja nie jest jeszcze dostępna!')
                input('Naciśnij Enter ...')
                self.pokaz_menu(menu)
            elif len(akcja) == 3:
                akcja[2]()
            else:
                akcja[2](akcja[3])
            

    def pokaz_menu_glowne(self):
        menu = {'tytul':'MENU', 'opcje': []}
        for i, pomieszczenie in enumerate(self.posiadlosc.pomieszczenia):
            menu['opcje'].append([
                i+1,
                pomieszczenie.nazwa,
                self.pokaz_menu_pomieszczenia,
                pomieszczenie
            ])

        menu['opcje'].append(['s', 'Zapis i wyjście', self.zapisz_zakoncz])
        menu['opcje'].append(['q', 'Wyjście bez zapisu', self.zakoncz])
        menu['opcje'].append(['0', 'Resetuj program', self.reset_programu])
        self.pokaz_menu(menu)
   

    def zapisz_zakoncz(self):
        print('zapisuje i koncze...')
        with open('saveOgrodPlik.pkl', 'wb') as saveOgrod:
            pickle.dump(self.posiadlosc, saveOgrod)

        
    def zakoncz(self):
        print('koncze...')
 
 
    def reset_programu(self):
        pyt = input('Czy jestes pewien - jeśli tak napisz TAK ')
        if pyt == 'TAK' :
            try:
                os.remove("saveOgrodPlik.pkl")
            except:
                pass
        else:
            self.pokaz_menu_glowne()


    def wyswietl_pomieszczenie(self, pomieszczenie):
        self.cls()
        print(pomieszczenie)
        input('Naciśnij Enter ...')
        self.pokaz_menu_pomieszczenia(pomieszczenie)
        

    def dodaj_przedmiot(self, pomieszczenie):
        nazwa = input('Podaj nazwę przedmiotu: ')
        gwarancja = input('data ważności?: ')
        opis = input('Podaj opis przedmiotu: ')
        typ = input('Narzędzia - n , nasiona - s: ')
        if typ == 'n':
            przedmiot = Narzedzie(pomieszczenie, nazwa, opis, gwarancja)
        elif typ == 's':
            przedmiot = Nasiona(pomieszczenie, nazwa, opis, gwarancja)
        self.pokaz_menu_pomieszczenia(pomieszczenie)

    def dodaj_rosline(self, pomieszczenie):
        nazwa = input('Podaj nazwę rośliny: ')
        rodzaj = input('Podaj rodzaj rośliny: ')
        ile_roslin = input('Podaj ilość roślin: ')
        co_ile_podlewac = input('Jak często podlewać?: ')
        kiedy_ostatnio_p = input('Kiedy ostatnio podlano?: ')
        jak_dlugo_podlewac = input('Jak długo podlewać?: ')
        kiedy_zbierac = input('Kiedy zbierać?: ')
        obszar = input('w jakim obszaże się znajduje?: ')
        
        rosliny = Rosliny(pomieszczenie, nazwa, rodzaj ,ile_roslin, co_ile_podlewac, kiedy_ostatnio_p, jak_dlugo_podlewac, kiedy_zbierac ,obszar)
                 
        self.pokaz_menu_pomieszczenia(pomieszczenie)


    def usun_przedmiot(self, pomieszczenie):
        przedmiot_do_dek = input('Podaj nazwę przedmiotu do usunięcia lub 0 - anuluj ')
        if przedmiot_do_dek != '0':
            if pomieszczenie.usun_przedmiot(przedmiot_do_dek):
                print('usunięto', przedmiot_do_dek)
            input('Naciśnij Enter ...')
        self.pokaz_menu_pomieszczenia(pomieszczenie)


    def usun_rosline(self, pomieszczenie):
        roslina_do_dek = input('Podaj nazwę rośliny do usunięcia lub 0 - anuluj ')
        if roslina_do_dek != '0':
            if pomieszczenie.usun_rosline(roslina_do_dek):
                print('usunięto', roslina_do_dek)
            input('Naciśnij Enter ...')
        self.pokaz_menu_pomieszczenia(pomieszczenie)

##################################################################

    def pokaz_menu_pomieszczenia(self, pomieszczenie):
        menu = {'tytul':'MENU '+pomieszczenie.nazwa.upper(), 'opcje': []}
        
        nr_opcji = 0
        if pomieszczenie.pomieszczenia:
            for podpomieszczenie in pomieszczenie.pomieszczenia:
                nr_opcji += 1
                menu['opcje'].append([nr_opcji, podpomieszczenie.nazwa, self.pokaz_menu_pomieszczenia, podpomieszczenie])
        if pomieszczenie.czy_narzedzia:
            nr_opcji += 1
            menu['opcje'].append([nr_opcji, 'Wyświetl listę przedmiotów', self.wyswietl_pomieszczenie, pomieszczenie])
            nr_opcji += 1
            menu['opcje'].append([nr_opcji, 'Dodaj przedmiot', self.dodaj_przedmiot, pomieszczenie])
            nr_opcji += 1
            menu['opcje'].append([nr_opcji, 'Usuń przedmiot', self.usun_przedmiot, pomieszczenie])
        if pomieszczenie.czy_rosliny:
            nr_opcji += 1
            menu['opcje'].append([nr_opcji, 'Wyświetl listę roślin', self.wyswietl_pomieszczenie, pomieszczenie])
            nr_opcji += 1
            menu['opcje'].append([nr_opcji, 'Dodaj roślinę', self.dodaj_rosline, pomieszczenie])
            nr_opcji += 1
            menu['opcje'].append([nr_opcji, 'Usuń roślinę', self.usun_rosline, pomieszczenie])
        
        
        menu['opcje'].append(['0', 'Powrót do menu głównego', self.pokaz_menu_glowne])
        
        akcja = self.pokaz_menu(menu)
