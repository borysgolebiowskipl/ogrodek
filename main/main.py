import pickle
from posiadlosc import *
from menu import Menu 
import os.path

def buduj_baze_poczatkowa():
    posiadlosc = Posiadlosc()
    altanka       = posiadlosc.dodaj_pomieszczenie('altanka')
    narzedziownia = altanka.dodaj_pomieszczenie('narzedziownia', czy_narzedzia=True)
    strych        = altanka.dodaj_pomieszczenie('strych', czy_narzedzia=True)
    ogrod         = posiadlosc.dodaj_pomieszczenie('ogrod', czy_rosliny=True)

    mlotek = Przedmiot(narzedziownia, 'mlotek', 'Przedmiot do wbijania gwozdzi', pobrane=True)
    lopata = Przedmiot(narzedziownia, 'lopata', 'Przedmiot do kopania')
    obcegi = Przedmiot(narzedziownia,'obcegi', 'Przedmiot do wyrywania gwozdzi')
    Nasiona_Pomidory = Nasiona(strych, 'Nasiona_Pomidory', 'Nasiona pomidorow!', '2022', pobrane=True)
    Nasiona_Ziemniaki = Nasiona(strych, 'Nasiona_Ziemniaki', 'Nasiona zmiemniakow', '2023')
    Nasiona_Buraki = Nasiona(strych, 'Nasiona_Buraki', 'Nasiona burakow', '2024')
    Nasiona_tulipany = Nasiona(strych, 'Nasiona_tulipany', 'Nasiona tulipanow', '2024')
    ziemniaki = Rosliny(ogrod, 'ziemniaki', 'Warzywa', 100, 'co tydzien', '05-05-2021', '0.5h', '18-07-2021', '1')
    buraki = Rosliny(ogrod ,'buraki', 'Warzywa', 20, 'co tydzien', '05-05-2021', '0.5h', '15-04-2021' , '1')
    marchewki = Rosliny(ogrod ,'marchewki', 'Warzywa', 30, 'co tydzien', '05-05-2021', '0.5h', '19-05-2021', '1')
    roze = Rosliny(ogrod, 'roze', 'Kwiaty', 15, 'co tydzien', '05-05-2021', '0.5h', '20-06-2021', '2')
    tulipany = Rosliny(ogrod ,'tulipany', 'Kwiaty', 20, 'co tydzien', '12-05-2021', '0.5h', '21-10-2021', '2')
    stokrotki = Rosliny(ogrod ,'stokrotki', 'Kwiaty', 40, 'co tydzien', '25-05-2021', '0.5h', '22-06-2021', '2')
    trawaPolna = Rosliny(ogrod ,'trawaPolna', 'Trawa', 1, 'co tydzien', '15-05-2021', '0.5h', '25-07-2021', '2')
    jablon = Rosliny(ogrod ,'jablon', 'Drzewa', 1, 'co miesiac', '05-05-2021', '1h', '22-07-2021', '3')
    grusza = Rosliny(ogrod ,'grusza', 'Drzewa', 2, 'co miesiac', '04-05-2021', '2h', '23-09-2021' , '3')
    porzeczki = Rosliny(ogrod ,'porzeczki', 'Krzewy', 2, 'co tydzien', '14-06-2021', '1h', '23-09-2021' , '4')
    agrest = Rosliny(ogrod ,'agrest', 'Krzewy', 2, 'co tydzien', '14-06-2021', '1h', '23-09-2021' , '4')
    return posiadlosc

if __name__ == '__main__':

    if os.path.isfile('saveOgrodPlik.pkl'):
        try:
            with open('saveOgrodPlik.pkl', 'rb') as loadOgrod:
                posiadlosc = pickle.load(loadOgrod)
        except:
            print('Wystąpił błąd przy wczytywaniu bazy danych!')
            posiadlosc = buduj_baze_poczatkowa()

    else:
        posiadlosc = buduj_baze_poczatkowa()

    menu = Menu(posiadlosc)
    menu.pokaz_menu_glowne()


