from tabulate import tabulate


class Ogrod:
    def __init__(self):
        self.calkowita_powieszchnia_w_m3 = 1000 # teren ogrodu (obszar, obszary do uzupelnienia)

class Przedmiot:
    def __init__(self, pomieszczenie, nazwa, opis, dataWaznosci=None, pobrane=False):
        self.nazwa = nazwa
        self.opis = opis
        self.dataWaznosci = dataWaznosci or 'brak'
        self.pobrane = pobrane
        pomieszczenie.przedmioty.append(self)

    def __str__(self):
        return 'nazwa: {}, opis: {}, gwarancja do: {}'.format(self.nazwa,  self.opis , self.dataWaznosci)

class Nasiona(Przedmiot):
    pass

class Narzedzie(Przedmiot):
    pass


class Rosliny():
    def __init__(self, pomieszczenie, nazwa , rodzaj ,ile_roslin=1 ,  co_ile_podlewac=None , kiedy_ostatnio_p=None ,
                 jak_dlugo_podlewac=None , kiedy_zbierac=None , obszar = None ):
        self.nazwa = nazwa
        self.ile_roslin = ile_roslin
        self.co_ile_podlewac = co_ile_podlewac
        self.kiedy_ostatnio_p = kiedy_ostatnio_p
        self.jak_dlugo_podlewac = jak_dlugo_podlewac
        self.kiedy_zbierac = kiedy_zbierac
        self.rodzaj = rodzaj
        self.obszar = obszar
        pomieszczenie.rosliny.append(self)

    def __str__(self):
        return 'nazwa: {}, rodzaj: {}, ile_roslin: {}'.format(self.nazwa,  self.rodzaj , self.ile_roslin)


class Pomieszczenie:
    '''gdzie przedmiot lezy'''
    def __init__(self, nazwa, czy_narzedzia=False, czy_rosliny=False):
        self.nazwa = nazwa
        self.przedmioty = []
        self.rosliny = []
        self.czy_narzedzia = czy_narzedzia
        self.czy_rosliny = czy_rosliny
        self.pomieszczenia = []



    def __str__(self):
        if self.czy_narzedzia:
            naglowek = ['Nazwa', 'Opis', 'Data ważności']
            ret = ' Przedmioty w {}:\n'.format(self.nazwa)
            dane = []
            for przedmiot in self.przedmioty:
                if not przedmiot.pobrane:
                    dane.append([przedmiot.nazwa, przedmiot.opis, przedmiot.dataWaznosci])
            ret += tabulate(dane, headers=naglowek, tablefmt="fancy_grid") + '\n\n'
            
            ret += ' Przedmioty pobrane z {}:\n'.format(self.nazwa)    
            dane = []
            for przedmiot in self.przedmioty:
                if przedmiot.pobrane:
                    dane.append([przedmiot.nazwa, przedmiot.opis, przedmiot.dataWaznosci])
            ret += tabulate(dane, headers=naglowek, tablefmt="fancy_grid") + '\n\n'
            
        if self.czy_rosliny:
            naglowek = ['Nazwa', 'Rodzaj', 'Ile roślin', 'Co ile podlewać', 'Kiedy podlane', 'Jak długo podl.', 'Kiedy zbierać' , 'Obszar']
            ret = ' Rosliny w {}:\n'.format(self.nazwa)
            dane = []
            for roslina in self.rosliny:
                    dane.append([roslina.nazwa, roslina.rodzaj, roslina.ile_roslin, roslina.co_ile_podlewac, 
                        roslina.kiedy_ostatnio_p, roslina.jak_dlugo_podlewac, roslina.kiedy_zbierac , roslina.obszar])
            ret += tabulate(dane, headers=naglowek, tablefmt="fancy_grid") + '\n\n'

        return ret


    def znajdz_przedmiot(self, nazwa):
        for przedmiot in self.przedmioty:
            if przedmiot.nazwa == nazwa:
                return przedmiot
        return None

    def usun_przedmiot(self, nazwa):
        przedmiot = self.znajdz_przedmiot(nazwa)
        if przedmiot: # jezeli przedmiotu nie znaleziono to przedmiot = None
            self.przedmioty.remove(przedmiot)
            return True
        else:
            print('brak na liscie ' , nazwa)
            return False

    def znajdz_rosline(self, nazwa):
        for roslina in self.rosliny:
            if roslina.nazwa == nazwa:
                return roslina
        return None

    def usun_rosline(self, nazwa):
        roslina = self.znajdz_rosline(nazwa)
        if roslina:
            self.rosliny.remove(roslina)
            return True
        else:
            print('brak na liscie ' , nazwa)
            return False

    def dodaj_pomieszczenie(self, nazwa, czy_narzedzia=False, czy_rosliny=False):
        pomieszczenie = Pomieszczenie(nazwa, czy_narzedzia, czy_rosliny)
        self.pomieszczenia.append(pomieszczenie)
        pomieszczenie.rodzic = self
        return pomieszczenie
        
    def pomieszczenie(self, nazwa):
        for pomieszczenie in self.pomieszczenia:
            if pomieszczenie.nazwa == nazwa:
                return pomieszczenie
        return None


class Posiadlosc:
    def __init__(self):
        self.pomieszczenia = []
        
    def dodaj_pomieszczenie(self, nazwa, czy_narzedzia=False, czy_rosliny=False):
        pomieszczenie = Pomieszczenie(nazwa, czy_narzedzia, czy_rosliny)
        self.pomieszczenia.append(pomieszczenie)
        pomieszczenie.rodzic = self
        return pomieszczenie
        
    def pomieszczenie(self, nazwa):
        for pomieszczenie in self.pomieszczenia:
            if pomieszczenie.nazwa == nazwa:
                return pomieszczenie
        return None
