#Tworze ogrod tescia wyrazony classami w python


import pickle

class Ogrod:
    calkowita_powieszchnia_w_m3 = 1000  # teren ogrodu (obszar, obszary do uzupelnienia)


lista_obiektow = []  # lista która ma oddawać obiekty utworzone dla użytku pickle - problem jest takie ze ona zawsze się zeruje podczas uruchomienia programu wiec po drugim/trzecim uruchomieniu programu pusta lista będzie naspisywać saveOgrodPlik.pkl
lista_obiektow_unpicle_list = []

class Narzedzia:

    def __init__(self, co, opis='nd', dataWaznosci='nd'):  # kostruktor
        self.co = co
        self.opis = opis
        self.info = co + 'sztuk:' + 'jest w:', opis
        self.dataWaznosci = dataWaznosci

    def fullinfo(self):
        return 'co:{} opis:{} , gwarancja do:{}'.format(self.co, self.opis, self.dataWaznosci)

class Nasiona(Narzedzia):
    pass
################################################
class Rosliny:
    def __init__(self, co , ile_roslin=1 ,  co_ile_podlewac='nd' , kiedy_ostatnio_p='nd' ,
                 jak_dlugo_podlewac='nd' , kiedy_zbierac='nd' ):
        self.co = co
        self.ile_roslin = ile_roslin
        self.co_ile_podlewac = co_ile_podlewac
        self.kiedy_ostatnio_p = kiedy_ostatnio_p
        self.jak_dlugo_podlewac = jak_dlugo_podlewac
        self.kiedy_zbierac = kiedy_zbierac




    def wyswietl_kiedy_podlewac (self):
        print('podalano ostanio  ' , ' ', self.kiedy_ostatnio_p, 'a podlewac ' , self.jak_dlugo_podlewac)


    def wyswietl_kiedy_zbierac (self):
        print('zbierac ' , ' ', self.kiedy_zbierac, 'po ' , self.kiedy_zasadzono)

    def zmien_ile_roslin(self , ile_roslin):
        self.ile_ile_roslin = ile_roslin

    def zmien_kiedy_ostynio_podlewane(self , kiedy_ostatnio_p):
        self.kiedy_ostatnio_p = kiedy_ostatnio_p

    def zmien_kiedy_zasadzono(self , kiedy_zasadzono):
        self.kiedy_zasadzono = kiedy_zasadzono

    def wyswietl_o_roslinie_full(self):
        print(  'co:',  self.co ,  '\nile roslin: ' , self.ile_roslin ,'\nco ile podlewac:'  , self.co_ile_podlewac,
                '\nkiedy ostatnio podlewane: '   , self.kiedy_ostatnio_p,'\njak dlugo podlewac: ' , self.jak_dlugo_podlewac,
                '\nkiedy zbierac:' , self.kiedy_zbierac , end =" " )

        # if   Obszar.obszar1 != None: print(self.obszar1)
        # elif Obszar.obszar2 != None: print(self.obszar2)
        # elif Obszar.obszar3 != None: print(self.obszar3)
        # elif Obszar.obszar4 != None: print(self.obszar4)
        # else: print('obszar brak info')








#############


class Warzywa(Rosliny):
    pass
class Kwiaty(Rosliny):
    pass
class Trawa(Rosliny):
    pass
class Drzewa(Rosliny):
    pass
class Krzewy(Rosliny):
    pass


##################################OBSZARY TO KLASA POMIESZCZENIE A BIEKTY TO :OBSZAR1 OBSZAR2...
class Obszar:
    def __init__(self, nazwa,  obszar1=None ,obszar2=None , obszar3=None , obszar4=None):
        '''gdzie roslina jest'''
        self.nazwa = nazwa
        if not obszar1:
            self.obszar1 = []
        else:
            self.obszar1 = obszar1

        if not obszar2:
            self.obszar2 = []
        else:
            self.obszar2 = obszar2

        if not obszar3:
            self.obszar3 = []
        else:
            self.obszar3 = obszar3

        if not obszar4:
            self.obszar4 = []
        else:
            self.obszar4 = obszar4


    def print_przedniot(self):
        print('#####Przedmioty w {}:'.format(self.nazwa))
        for przedioty in self.przedioty:
            print('-->', przedioty.fullinfo())

        for przedioty in self.przediotyPobrane:
            print('#####Przedmioty pobrane z {}:'.format(self.nazwa))
            print('-->', przedioty.fullinfo())
            print('***************************************')

    def pobierz_przedniot(self, przedioty):
        if not przedioty in self.przediotyPobrane:
            self.przediotyPobrane.append(przedioty)
        if przedioty in self.przedioty:
            self.przedioty.remove(przedioty)

    def oddaj_przedniot(self, przedioty):
        if not przedioty in self.przedioty:
            self.przedioty.append(przedioty)
        if przedioty in self.przediotyPobrane:
            self.przediotyPobrane.remove(przedioty)

    def usun_przedniot(self, przedioty):
        if przedioty in self.przedioty:
            self.przedioty.remove(przedioty)
        if przedioty in self.przediotyPobrane:
            self.przediotyPobrane.remove(przedioty)


#pbszary to pomieszczenie
###############
#################################################
class Pomieszczenie:
    '''gdzie narzedzie lezy'''

    def __init__(self, nazwa, przedioty=None, przediotyPobrane=None):

        self.nazwa = nazwa
        if not przedioty:
            self.przedioty = []
        else:
            self.przedioty = przedioty
        if not przediotyPobrane:
            self.przediotyPobrane = []
        else:
            self.przediotyPobrane = przediotyPobrane


    def print_przedniot(self):
        print('#####Przedmioty w {}:'.format(self.nazwa))
        for przedioty in self.przedioty:
            print('-->', przedioty.fullinfo())

        for przedioty in self.przediotyPobrane:
            print('#####Przedmioty pobrane z {}:'.format(self.nazwa))
            print('-->', przedioty.fullinfo())
            print('***************************************')

    def pobierz_przedniot(self, przedioty):
        if not przedioty in self.przediotyPobrane:
            self.przediotyPobrane.append(przedioty)
        if przedioty in self.przedioty:
            self.przedioty.remove(przedioty)

    def oddaj_przedniot(self, przedioty):
        if not przedioty in self.przedioty:
            self.przedioty.append(przedioty)
        if przedioty in self.przediotyPobrane:
            self.przediotyPobrane.remove(przedioty)

    def usun_przedniot(self, przedioty):
        if  przedioty in self.przedioty:
            self.przedioty.remove(przedioty)
        if przedioty in self.przediotyPobrane:
            self.przediotyPobrane.remove(przedioty)



try:
    with open('saveOgrodPlik.pkl', 'rb') as loadOgrod:
        lista_obiektow_unpicle_list = pickle.load(loadOgrod)  # lista_obiektow_unpicle_list obiektow
    lista_obiektow = lista_obiektow_unpicle_list
    narzedziownia, strych = lista_obiektow      #przypisuje do 2ch zmiennych dwa obiekty z listy

except:
    narzedziownia = Pomieszczenie('narzedzionaia')
    strych = Pomieszczenie('strych')
    mlotek = Narzedzia('mlotek', 'mloteczek!!!', 'mlot2021')
    lopata = Narzedzia('lopata', 'lopatka!!!')
    obcegi = Narzedzia('obcegi', 'obcegi2e')
    Pomidory = Nasiona('Pomidory', 'Pomidory2', 'Pomidory2022')
    Ziemniaki = Nasiona('Ziemniaki', 'Ziemniaki2', 'Ziemniaki2023')
    Buraki = Nasiona('Buraki', 'Buraki2', 'Buraki2024')
    narzedziownia.oddaj_przedniot(mlotek)
    narzedziownia.pobierz_przedniot(mlotek)
    narzedziownia.oddaj_przedniot(lopata)
    narzedziownia.oddaj_przedniot(obcegi)
    strych.oddaj_przedniot(Pomidory)
    strych.oddaj_przedniot(Ziemniaki)
    strych.oddaj_przedniot(Buraki)
    strych.pobierz_przedniot(Pomidory)
    lista_obiektow = [narzedziownia, strych]
    ziemniaki = Warzywa('ziemniaki', 100, 'co tydzien', '05-05-2021', '0.5h', '18-07-2021')
    buraki = Warzywa('buraki', 20, 'co tydzien', '05-05-2021', '0.5h', '15-04-2021')
    marchewki = Warzywa('marchewki', 30, 'co tydzien', '05-05-2021', '0.5h', '19-05-2021')
    roze = Kwiaty('roze', 15, 'co tydzien', '05-05-2021', '0.5h', '20-06-2021')
    tulipany = Kwiaty('tulipany', 20, 'co tydzien', '12-05-2021', '0.5h', '21-10-2021')
    stokrotki = Kwiaty('stokrotki', 40, 'co tydzien', '25-05-2021', '0.5h', '22-06-2021')
    trawaPolna = Trawa('trawaPolna', 1, 'co tydzien', '15-05-2021', '0.5h', '25-07-2021')
    jablon = Drzewa('jablon', 1, 'co miesiac', '05-05-2021', '1h', '22-07-2021')
    grusza = Drzewa('grusza', 2, 'co miesiac', '04-05-2021', '2h', '23-09-2021')

############################################################################################################

def menu_glowne():
    print('''
1. Przedmioty
2. Rosliny
s. Zapis i wyjscie
q. Wyjscie bez zapisu
0. Resetuj program                                            #usuwa plik 

    ''')
    while True:
        akcja = input('Podaj akcje do wykoanaia:')
        if akcja == '1':
            return menu_przedmioty()


        if akcja == 's':
            print('zaoisuje i koncze...')

            with open('saveOgrodPlik.pkl', 'wb') as saveOgrod:
                pickle.dump(lista_obiektow, saveOgrod)  # zapisuje do pliku lise obeiektow
            break

        if akcja == 'q':
            return print('koncze...')






def menu_przedmioty():
    while True:
        print(''''
********************MENU**************************
1. Wyswietl liste przedniotow
2. Dodaj przedmiot
3. Usun przedmiot
0. Powrot do menu glownego
**************************************************
''')
        while True:
            akcja = input('Podaj akcje do wykoanaia:')
            if akcja == '1':
                print('1. narzedzionaia')
                print('2. strych')
                akcja2 = input('Podaj miejsce:')
                if akcja2 == '1':
                    narzedziownia.print_przedniot()
                    break

                if akcja2 == '2':
                    strych.print_przedniot()
                    break

            if akcja == '2':
                while True:
                    nazwaPrzedmiot = input('Podaj nazwe przedmiotu')
                    gwarancja = input('data waznosci?: ')
                    opisPrzedmiot = input('podaj opis przediotu')

                    while True:
                        typPrzedmiot = input('Narzedzia - n , nasiona - s')
                        if typPrzedmiot == 'n' or typPrzedmiot == 's':
                            if typPrzedmiot == 'n':
                                nazwaPrzedmiot = Narzedzia(nazwaPrzedmiot, opisPrzedmiot, gwarancja)
                                narzedziownia.oddaj_przedniot(nazwaPrzedmiot)
                                break
                            if typPrzedmiot == 's':
                                nazwaPrzedmiot = Nasiona(nazwaPrzedmiot, opisPrzedmiot, gwarancja)
                                strych.oddaj_przedniot(nazwaPrzedmiot)
                                break
                    break

            if akcja == '3':
                while True:
                    print('lista przedmiotow :' , narzedziownia.print_przedniot())
                    nazwa_przedmiotusun = input('Podaj nazwe przedmiotu dom usuniecia')
                    print(nazwa_przedmiotusun)
                    del nazwa_przedmiotusun
                    print('lista przedmiotow :', narzedziownia.print_przedniot())  #wiesz do sprawdzenia poprawnosci i do del

                    break



            if akcja == '0':
                break
        break
    menu_glowne()




def menu_rosliny():
    while True:
        print(''''
********************MENU**************************
1. Wyswietl liste Roslin
2. Dodaj rosline
3. Usun rosline
4. Gdzie rosnie roslina
0. Powrot do menu glownego
**************************************************
''')


        while True:
            akcja = input('Podaj akcje do wykoanaia:')
            if akcja == '1':
                print('1. obszar1')
                print('2. obszar2')
                print('3. obszar3')
                print('4. obszar4')
                print('5. wszystkie obszary')
                akcja2 = input('Podaj miejsce:')
                if akcja2 == '1':
                    obszar1.print_przedniot()
                    break
                if akcja2 == '2':
                    strych.print_przedniot()
                    break
            if akcja == '2':
                while True:
                    nazwaPrzedmiot = input('Podaj nazwe przedmiotu')
                    gwarancja = input('data waznosci?: ')
                    opisPrzedmiot = input('podaj opis przediotu')

                    while True:
                        typPrzedmiot = input('Narzedzia - n , nasiona - s')
                        if typPrzedmiot == 'n' or typPrzedmiot == 's':
                            if typPrzedmiot == 'n':
                                nazwaPrzedmiot = Narzedzia(nazwaPrzedmiot, opisPrzedmiot, gwarancja)
                                narzedziownia.oddaj_przedniot(nazwaPrzedmiot)
                                break
                            if typPrzedmiot == 's':
                                nazwaPrzedmiot = Nasiona(nazwaPrzedmiot, opisPrzedmiot, gwarancja)
                                strych.oddaj_przedniot(nazwaPrzedmiot)
                                break
                    break

            if akcja == '3':
                while True:
                    print('lista przedmiotow :' , narzedziownia.print_przedniot())
                    nazwaPrzedmiotusun = input('Podaj nazwe przedmiotu dom usuniecia')
                    del nazwaPrzedmiotusun
                    print('lista przedmiotow :', narzedziownia.print_przedniot())  #wiesz do sprawdzenia poprawnosci i do del
                    break
            if akcja == '0':
                break
        break
    menu_glowne()













menu_glowne()